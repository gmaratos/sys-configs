" ~/.vimrc

"basic behavior and look
syntax on
colorscheme default
set guicursor=i:block
set relativenumber showcmd hidden background=light
set mouse=a

"set indent behavior
set softtabstop=4 shiftwidth=4 expandtab
set autoindent
filetype indent off

"highlighting
"match trailing spaces except when cursor is there
"redraw on exit of insert mode

highlight ws ctermbg=grey guibg=grey
match ws /\s\+\%#\@<!$/ "
autocmd InsertLeave * redraw!
  
