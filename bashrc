# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions

alias ls='ls --color=auto'
alias virtualenv='python -m venv --upgrade-deps'
alias tmux='tmux -2' #force 256 color mode
alias zathura='zathura --fork --log-level=error'

export GPG_TTY=$(tty) #for gpg encrypt pipeline
unset DEBUGINFOD_URLS
alias gdb='gdb -q'

#file sync

sync_prefix='/home/c3lnp9k57/Desktop'
global_options=( '-auv' '--exclude .git/' '--exclude .gitignore' '--exclude .venv/')
remote='fernando'

alias ferpull='rsync ${global_options[@]} $remote:/srv/files/ $sync_prefix/files/'
alias ferpush='rsync ${global_options[@]} $sync_prefix/files/ $remote:/srv/files/'
alias check_ferpull='rsync -n ${global_options[@]} $remote:/srv/files/ $sync_prefix/files/'
alias check_ferpush='rsync -n ${global_options[@]} $sync_prefix/files/ $remote:/srv/files/'

# unset debuginfod

unset DEBUGINFOD_URLS

# shell prompt

#generated prompt with https://scriptim.github.io/bash-prompt-generator/
export PS1='\[\e[0;1;38;5;30m\]\u\[\e[0m\]@\[\e[0m\][\[\e[0;1;91m\]\H\[\e[0m\]]\[\e[0m\]:\[\e[0;2;34m\]\w\n\[\e[0m\]\$\[\e[0m\] \[\e[0m\]'
